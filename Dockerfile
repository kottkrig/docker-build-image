FROM elixir:1.6

RUN apt-get update && apt-get install -y bash openssl curl sudo openssh-client make git g++
RUN curl --silent --location https://deb.nodesource.com/setup_8.x | sudo bash -
RUN apt-get install -y nodejs


RUN mix local.hex --force && \
    mix local.rebar --force
